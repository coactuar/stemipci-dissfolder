-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 05:33 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stemipci_faculty`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(50) NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `hospital` varchar(150) NOT NULL,
  `User Action` varchar(200) NOT NULL,
  `eventname` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `FullName`, `email`, `city`, `hospital`, `User Action`, `eventname`) VALUES
(1, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', '04-03-2021 18:57', '04-03-2021 20:46'),
(2, 'Gopala Koduru', 'gopalkoduru@gmail.com', 'Vijayawada', 'Aayush Hospital ', '04-03-2021 18:24', '04-03-2021 20:45'),
(3, 'Jebin John', 'jebin.john229@jtb-india.com', 'Bengaluru', 'JTB India Pvt. Ltd.', '04-03-2021 18:57', '04-03-2021 20:48'),
(4, 'PLNKapardhi', 'dr.kaparthi@gmail.c', 'Hyderabad', 'care hospitals', '04-03-2021 18:57', '04-03-2021 20:46'),
(5, 'Dr Kaushal Chhatrapati', 'drkaushalc@gmail.com', 'Mumbai', 'Sir HN Reliance Foundation Hospital', '04-03-2021 18:45', '04-03-2021 20:46'),
(6, 'Biju Govind', 'drbijugovind@yahoo.co.in', 'GUNTUR', 'NRI MEDICAL COLLEGE', '04-03-2021 18:57', '04-03-2021 20:19'),
(7, 'Sudheer', 'sudhir8in@yahoo.co.in', 'Hyderabad', 'Citizens', '04-03-2021 18:57', '04-03-2021 20:46'),
(9, 'BHARAT', 'dr.bvp1@gmail.com', 'hyderabad', 'care hospital', '04-03-2021 19:02', '04-03-2021 19:40'),
(10, 'BHARAT', 'dr.bvp1@gmail.com', 'hyderabad', 'care hospital', '04-03-2021 19:54', '04-03-2021 20:46'),
(11, 'dr sarat chandra k', 'saratkoduganti@gmail.com', 'hyderabad', 'virinchi hospital', '04-03-2021 19:21', '04-03-2021 20:46'),
(12, 'Bharat Purohit', 'dr.bvp1@gmail.com', 'Hyderabad', 'Care hospital', '04-03-2021 19:51', '04-03-2021 19:52'),
(13, 'Bharat Purohit', 'dr.bvp1@gmail.com', 'Hyderabad', 'Care hospital', '04-03-2021 19:54', '04-03-2021 19:55'),
(14, 'Biju Govind', 'drbijugovind@yahoo.co.in', 'GUNTUR', 'NRI MEDICAL COLLEGE', '04-03-2021 20:21', '04-03-2021 20:27'),
(15, 'Biju Govind', 'drbijugovind@yahoo.co.in', 'GUNTUR', 'NRI MEDICAL COLLEGE', '04-03-2021 20:28', '04-03-2021 20:46'),
(16, '', '', '', '', '', ''),
(17, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'test', NULL, NULL, '2021-03-02 18:16:28', '2021-03-02 18:16:28', '2021-03-02 18:34:37', 1, 'Abbott'),
(2, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-02 18:17:21', '2021-03-02 18:17:21', '2021-03-02 18:17:51', 1, 'Abbott'),
(3, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-02 18:20:48', '2021-03-02 18:20:48', '2021-03-02 18:21:18', 1, 'Abbott'),
(4, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-02 18:22:14', '2021-03-02 18:22:14', '2021-03-02 18:22:44', 1, 'Abbott'),
(5, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-02 18:33:50', '2021-03-02 18:33:50', '2021-03-02 18:34:20', 1, 'Abbott'),
(6, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-03 14:48:07', '2021-03-03 14:48:07', '2021-03-03 14:48:37', 1, 'Abbott'),
(7, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-03 17:32:51', '2021-03-03 17:32:51', '2021-03-03 17:33:21', 1, 'Abbott'),
(8, 'JTBTest ', 'udit.mathur144@gmail.com', 'Gurgaon', 'JTBTest', NULL, NULL, '2021-03-04 17:43:49', '2021-03-04 17:43:49', '2021-03-04 17:44:19', 1, 'Abbott'),
(9, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'hyderabad', 'AV', NULL, NULL, '2021-03-04 18:21:56', '2021-03-04 18:21:56', '2021-03-04 18:22:26', 1, 'Abbott'),
(10, 'Gopala Koduru', 'gopalkoduru@gmail.com', 'Vijayawada', 'Aayush Hospital ', NULL, NULL, '2021-03-04 18:24:55', '2021-03-04 18:24:55', '2021-03-04 18:25:25', 1, 'Abbott'),
(11, 'Jebin John', 'jebin.john229@jtb-india.com', 'Bengaluru', 'JTB India Pvt. Ltd.', NULL, NULL, '2021-03-04 18:26:11', '2021-03-04 18:26:11', '2021-03-04 18:26:41', 1, 'Abbott'),
(12, 'PLNKapardhi', 'dr.kaparthi@gmail.c', 'Hyderabad', 'care hospitals', NULL, NULL, '2021-03-04 18:37:13', '2021-03-04 18:37:13', '2021-03-04 18:37:43', 1, 'Abbott'),
(13, 'PLNKapardhi', 'dr.kaparthi@gmail.c', 'Hyderabad', 'care hospitals', NULL, NULL, '2021-03-04 18:37:13', '2021-03-04 18:37:13', '2021-03-04 18:37:43', 1, 'Abbott'),
(14, 'Ramesh Babu', 'pramesh.babu@abbott.com', 'Vijayawada', 'Abbott', NULL, NULL, '2021-03-04 18:38:28', '2021-03-04 18:38:28', '2021-03-04 18:38:58', 1, 'Abbott'),
(15, 'Dr Kaushal Chhatrapati', 'drkaushalc@gmail.com', 'Mumbai', 'Sir HN Reliance Foundation Hospital', NULL, NULL, '2021-03-04 18:45:33', '2021-03-04 18:45:33', '2021-03-04 18:46:03', 1, 'Abbott'),
(16, 'Biju Govind', 'drbijugovind@yahoo.co.in', 'GUNTUR', 'NRI MEDICAL COLLEGE', NULL, NULL, '2021-03-04 18:48:05', '2021-03-04 18:48:05', '2021-03-04 18:48:35', 1, 'Abbott'),
(17, 'Sudheer', 'sudhir8in@yahoo.co.in', 'Hyderabad', 'Citizens', NULL, NULL, '2021-03-04 18:50:15', '2021-03-04 18:50:15', '2021-03-04 18:50:45', 1, 'Abbott'),
(18, 'Biju Govind', 'drbijugovind@yahoo.co.in', 'guntur', 'NRI MEDICAL COLLEGE', NULL, NULL, '2021-03-04 18:51:39', '2021-03-04 18:51:39', '2021-03-04 18:52:09', 1, 'Abbott'),
(19, 'BHARAT', 'dr.bvp1@gmail.com', 'hyderabad', 'care hospital', NULL, NULL, '2021-03-04 19:01:10', '2021-03-04 19:01:10', '2021-03-04 19:01:40', 1, 'Abbott'),
(20, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-04 19:07:28', '2021-03-04 19:07:28', '2021-03-04 19:07:58', 1, 'Abbott'),
(21, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Kalyan', 'Abbott', NULL, NULL, '2021-03-04 19:08:50', '2021-03-04 19:08:50', '2021-03-04 19:09:20', 1, 'Abbott'),
(22, 'dr sarat chandra k', 'saratkoduganti@gmail.com', 'hyderabad', 'virinchi hospital', NULL, NULL, '2021-03-04 19:21:07', '2021-03-04 19:21:07', '2021-03-04 19:21:37', 1, 'Abbott'),
(23, 'Bharat Purohit', 'dr.bvp1@gmail.com', 'Hyderabad', 'Care hospital', NULL, NULL, '2021-03-04 19:51:55', '2021-03-04 19:51:55', '2021-03-04 19:52:25', 1, 'Abbott'),
(24, 'Bharat Purohit', 'dr.bvp1@gmail.com', 'Hyderabad', 'Care hospital', NULL, NULL, '2021-03-04 19:54:39', '2021-03-04 19:54:39', '2021-03-04 19:55:09', 1, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
