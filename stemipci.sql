-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 05:33 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stemipci`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Nishanth', 'nishanth@coact.co.in', 'HI', '2021-03-02 17:47:48', 'Abbott', 1, 1),
(2, 'Nishanth', 'nishanth@coact.co.in', 'hello', '2021-03-02 17:49:10', 'Abbott', 0, 1),
(3, 'Dr PS Srinivasa Chowdary', 'psschowdary@gmail.com', 'Comment : Door to wire time instead of DB time .After crossing with wire; balloon dottering; if flow doesn\'t improve local thrombosis .', '2021-03-04 20:11:28', 'Abbott', 1, 0),
(4, 'Dr PS Srinivasa Chowdary', 'psschowdary@gmail.com', 'Comment : If saline is used instead of Contrast ; 40 ml syringe rather than 20 ml syringe helps; low viscosity of saline compared to contrast matters', '2021-03-04 20:13:56', 'Abbott', 0, 1),
(5, 'Dr PS Srinivasa Chowdary', 'psschowdary@gmail.com', 'To Dr Sarath Chandra Sir  what\'s your experience Prasugrel 5 mg or 10 mg', '2021-03-04 20:23:19', 'Abbott', 0, 1),
(6, 'Dr PS Srinivasa Chowdary', 'psschowdary@gmail.com', 'To Dr Kaparthi Deferred Stenting by 48 hrs; what\'s your experience with GP|| B /III A versus only Heparin', '2021-03-04 20:25:10', 'Abbott', 1, 0),
(7, 'Dr PS Srinivasa Chowdary', 'psschowdary@gmail.com', 'Hi Kaushal', '2021-03-04 20:46:24', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-02 17:47:42', '2021-03-02 17:47:42', '2021-03-03 18:15:22', 0, 'Abbott'),
(2, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-02 17:49:03', '2021-03-02 17:49:03', '2021-03-03 18:15:22', 0, 'Abbott'),
(3, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-02 18:33:32', '2021-03-02 18:33:32', '2021-03-04 19:21:26', 0, 'Abbott'),
(4, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-03 09:55:08', '2021-03-03 09:55:08', '2021-03-04 19:21:26', 0, 'Abbott'),
(5, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-03 12:32:37', '2021-03-03 12:32:37', '2021-03-03 18:15:22', 0, 'Abbott'),
(6, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-03 16:33:50', '2021-03-03 16:33:50', '2021-03-03 18:15:22', 0, 'Abbott'),
(7, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-03 17:26:16', '2021-03-03 17:26:16', '2021-03-03 18:15:22', 0, 'Abbott'),
(8, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-03-03 17:29:34', '2021-03-03 17:29:34', '2021-03-03 18:15:22', 0, 'Abbott'),
(9, 'JTBTest ', 'udit.mathur144@jtb-india.com', 'Gurgaon', 'JTBTest', NULL, NULL, '2021-03-04 17:44:53', '2021-03-04 17:44:53', '2021-03-04 17:45:11', 0, 'Abbott'),
(10, 'V', 'viraj@coact.co.in', 'Fgj', 'Nh', NULL, NULL, '2021-03-04 18:10:50', '2021-03-04 18:10:50', '2021-03-04 18:11:20', 0, 'Abbott'),
(11, 'Dr PS Srinivasa Chowdary', 'psschowdary@gmail.com', 'Vijayawada, Andhra Pradesh', 'Aayush', NULL, NULL, '2021-03-04 18:26:52', '2021-03-04 18:26:52', '2021-03-04 20:46:18', 0, 'Abbott'),
(12, 'Dr Nirmal Kumar', 'drnirmal_kumar@yahoo.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-03-04 18:38:01', '2021-03-04 18:38:01', '2021-03-04 19:32:02', 0, 'Abbott'),
(13, 'Alluri srinivas raju', 'alluri32@gmail.com', 'Hyderabad ', 'Care hospital ', NULL, NULL, '2021-03-04 18:41:36', '2021-03-04 18:41:36', '2021-03-04 18:42:06', 0, 'Abbott'),
(14, 'Dr Deepthi', 'deepthi.kodati@gmail.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-04 18:50:06', '2021-03-04 18:50:06', '2021-03-04 18:51:06', 0, 'Abbott'),
(15, 'Dr Ravi', 'ravi.sangolkar@gmail.com', 'Hyderabad', 'Care Nampally', NULL, NULL, '2021-03-04 18:50:37', '2021-03-04 18:50:37', '2021-03-04 18:51:38', 0, 'Abbott'),
(16, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-04 18:51:00', '2021-03-04 18:51:00', '2021-03-04 20:12:38', 0, 'Abbott'),
(17, 'Dr Sanjeev Sengupta', 'drsanjeevsen@gmail.com', 'Hyderabad', 'Military Hospital', NULL, NULL, '2021-03-04 18:51:11', '2021-03-04 18:51:11', '2021-03-04 19:17:00', 0, 'Abbott'),
(18, 'Anil Chopra', 'anil.chopra@abbott.com', 'HYDERABAD', 'AV', NULL, NULL, '2021-03-04 18:51:36', '2021-03-04 18:51:36', '2021-03-04 20:46:26', 0, 'Abbott'),
(19, 'Ramesh Babu', 'pramesh.babu@abbott.com', 'Vijayawada', 'Abbott', NULL, NULL, '2021-03-04 18:55:54', '2021-03-04 18:55:54', '2021-03-04 20:46:55', 0, 'Abbott'),
(20, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'AV', NULL, NULL, '2021-03-04 18:59:40', '2021-03-04 18:59:40', '2021-03-04 19:55:14', 0, 'Abbott'),
(21, 'Kishore V R Akella', 'kishore.akella@abbott.com', 'Vizag', 'AV', NULL, NULL, '2021-03-04 19:02:21', '2021-03-04 19:02:21', '2021-03-04 19:19:23', 0, 'Abbott'),
(22, 'Dr P S Srinivasa Chowdary', 'psschowdary@gmail.com', 'Vijayawada, Andhra Pradesh', 'Aayush', NULL, NULL, '2021-03-04 19:03:09', '2021-03-04 19:03:09', '2021-03-04 20:46:18', 0, 'Abbott'),
(23, 'Chaitanya', 'chaitanya.vadlapatlas@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-03-04 19:03:22', '2021-03-04 19:03:22', '2021-03-04 19:22:52', 0, 'Abbott'),
(24, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-04 19:06:37', '2021-03-04 19:06:37', '2021-03-04 19:22:34', 0, 'Abbott'),
(25, 'Ramesh Babu', 'pramesh.babu@abbott.com', 'Vijayawada ', 'Abbott', NULL, NULL, '2021-03-04 19:08:54', '2021-03-04 19:08:54', '2021-03-04 20:46:55', 0, 'Abbott'),
(26, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-04 19:10:33', '2021-03-04 19:10:33', '2021-03-04 19:22:34', 0, 'Abbott'),
(27, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-04 19:10:57', '2021-03-04 19:10:57', '2021-03-04 20:12:38', 0, 'Abbott'),
(28, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-03-04 19:12:12', '2021-03-04 19:12:12', '2021-03-04 20:46:26', 0, 'Abbott'),
(29, 'sd', 'drshantanud@yahoo.com', 'Mumbai', 'JJ', NULL, NULL, '2021-03-04 19:13:21', '2021-03-04 19:13:21', '2021-03-04 20:28:52', 0, 'Abbott'),
(30, 'Sudarshan', 'sudarshanpalaparthi@gmail.com', 'Vijayawada', 'Aayush hospitals', NULL, NULL, '2021-03-04 19:16:31', '2021-03-04 19:16:31', '2021-03-04 19:22:02', 0, 'Abbott'),
(31, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-03-04 19:16:31', '2021-03-04 19:16:31', '2021-03-04 19:21:26', 0, 'Abbott'),
(32, 'Sravan', 'srb.sravan@gmail.com', 'Hyderabad ', 'Av', NULL, NULL, '2021-03-04 19:18:53', '2021-03-04 19:18:53', '2021-03-04 20:36:46', 0, 'Abbott'),
(33, 'Dr Hanumanth Reddy', 'drhanumanthareddycare@gmail.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-03-04 19:21:43', '2021-03-04 19:21:43', '2021-03-04 19:23:50', 0, 'Abbott'),
(34, 'Dr Ramchandra Raju', 'pvrcraju@gmail.com', 'Hyderabad', 'Star Hospital', NULL, NULL, '2021-03-04 19:23:37', '2021-03-04 19:23:37', '2021-03-04 19:25:19', 0, 'Abbott'),
(35, 'Dr Naveen Krishna', 'drnaveenkrishna@gmail.com', 'Hyderabad', 'Star Hospital', NULL, NULL, '2021-03-04 19:24:55', '2021-03-04 19:24:55', '2021-03-04 19:26:26', 0, 'Abbott'),
(36, 'Dr A N Patnaik', 'anpatnaik@yahoo.com', 'Hyderabad', 'Star Hospital', NULL, NULL, '2021-03-04 19:26:03', '2021-03-04 19:26:03', '2021-03-04 19:28:56', 0, 'Abbott'),
(37, 'Dr Meeraji Rao', 'meerajirao@yahoo.com', 'Hyderabad', 'Continental', NULL, NULL, '2021-03-04 19:28:31', '2021-03-04 19:28:31', '2021-03-04 19:30:04', 0, 'Abbott'),
(38, 'Dr Hari Ram', 'drhariram@citizenshospitals.com', 'Hyderabad', 'Citizens Hospital', NULL, NULL, '2021-03-04 19:29:49', '2021-03-04 19:29:49', '2021-03-04 19:31:34', 0, 'Abbott'),
(39, 'Dr V Vishwakranth', 'vishwakranth@gmail.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-03-04 19:31:07', '2021-03-04 19:31:07', '2021-03-04 19:32:19', 0, 'Abbott'),
(40, 'Nirmal Kumar', 'drnirmal_kumar@yahoo.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-03-04 19:31:55', '2021-03-04 19:31:55', '2021-03-04 19:32:02', 0, 'Abbott'),
(41, 'Prashanth ', 'docpk77@gmail.com', 'Hyd', 'Kindle ', NULL, NULL, '2021-03-04 19:34:18', '2021-03-04 19:34:18', '2021-03-04 19:46:18', 0, 'Abbott'),
(42, 'Sameer Pagad', 'spagad@yahoo.com', 'Thane west', 'Jupiter', NULL, NULL, '2021-03-04 20:08:42', '2021-03-04 20:08:42', '2021-03-04 20:19:07', 0, 'Abbott'),
(43, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-03-04 20:24:57', '2021-03-04 20:24:57', '2021-03-04 20:46:26', 0, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
